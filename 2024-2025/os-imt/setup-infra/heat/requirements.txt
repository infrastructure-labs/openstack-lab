#-e /home/bjonglez/code/enos
#-e /home/bjonglez/code/enoslib
enos==8.0.0a9
enoslib==8.4.0
ansible>=2.9,<2.10
click==7.1.2
python-heatclient>=2.5.0,<3.0
