#!/usr/bin/env bash

# Fix DNS resolution
echo "" > /etc/resolv.conf
echo "nameserver 8.8.8.8" >> /etc/resolv.conf

# Install figlet and lolcat
export DEBIAN_FRONTEND=noninteractive
apt update -q -y
apt install -y --force-yes figlet lolcat
