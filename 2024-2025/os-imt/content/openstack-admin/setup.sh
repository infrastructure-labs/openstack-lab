#!/usr/bin/env bash

set -o errexit
set -o xtrace

# Install the bare necessities
DEBIAN_FRONTEND=noninteractive apt install --yes --quiet curl tcpdump kmod vim htop lynx crudini coreutils

# Set the admin password
snap set microstack config.credentials.keystone-password=lab-os
# Automatically detect if KVM is usable
snap set microstack config.host.check-qemu=true
# Enable IP forward to provide network connectivity to VMs
snap set microstack config.host.ip-forwarding=true

# Initialize  OpenStack
microstack.init --auto --control

# Fix certificate verification
cp /var/snap/microstack/common/etc/ssl/certs/cacert.pem /usr/local/share/ca-certificates/microstack.crt
update-ca-certificates

# Install openstack client
snap info openstackclients | fgrep -q installed && sudo snap remove --purge openstackclients
snap install openstackclients --channel=ussuri/stable
