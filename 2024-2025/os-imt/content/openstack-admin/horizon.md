+++
title = "The Horizon dashboard"
weight = 2
+++

One service deployed is the OpenStack dashboard (Horizon). On your own
machine, horizon is reachable from the web browser at
`https://<ip-of-your-lab-machine>` with the following credentials:
- login: `admin`
- password: `lab-os`

From here, you can reach `Project > Compute > Instances > Launch
Instance` and boot a virtual machine given the following information:
- a name (e.g., `horizon-vm`)
- an image (e.g., `cirros`) and set the `Create New Volume` to "No"
- a flavor to limit the resources of your instance (we recommend
  `m1.tiny`)
- and a network setting (must be `test`)

You should select options by clicking on the big arrow on the right of
each possibility. When the configuration is OK, the `Launch Instance`
button should be enabled. After clicking on it, you should see the
instance in the `Active` state in less than a minute.

Now, you have several options to connect to your freshly deployed VM.
For instance, after clicking on its name, Horizon provides a virtual
console under the `Console` tab. So, you can use the following
credentials to access the VM:
- login: `cirros`
- password: `gocubsgo`

{% do() %}
Login to the VM using the virtual console from Horizon.  Determine the
network gateway of the VM using `ip route show` inside the VM.
Can you ping the gateway from the VM?
{% end %}

{% solution() %}
The gateway is normally `192.168.222.1`.

However, you might have trouble to ping it from the VM, simply because
it is difficult to enter the `.` character through the virtual console!
This is likely a mismatch between keyboard mappings, and a good
illustration why it's uncommon to use the virtual console through the web
interface.
{% end %}

As a *DevOps-in-progress*, you will prefer accessing your VM through the
command-line interface...
