+++
title = "Discovering Heat"
weight = 3
+++

**You will also find all Heat templates described below in the [lab-os
tarball](../../lab-os.tar.gz)**

## Boot a VM

The simplest HOT template you can declare describes how to boot a VM.

{{ includecode(path="@/openstack-user-heat/1-boot-vm.yaml", syntax="yaml") }}

As depicted in this example, the different OpenStack resources can be
declared using types.  OpenStack resource types are listed in the
[documentation](https://docs.openstack.org/heat/ussuri/template_guide/openstack.html),
browsing this page, you can see that resources exist for most OpenStack
services (e.g. Nova, Neutron, Glance, Cinder, Heat).  Here, we declare a
new resource called `heat-vm` which is defined by the type
`OS::Nova::Server` to declare a new virtual machine.  A type specifies
different properties (some are mandatory, some are optional, [see the
documentation](https://docs.openstack.org/heat/ussuri/template_guide/openstack.html)
for more details).  The `OS::Nova::Server` properties should be familiar
to you since it is the classical properties Nova requires to boot a VM
(i.e. name, image, flavor, key name).

Once you have written this template in a file, you can now deploy the
stack as following:

```bash
openstack stack create -t ./openstack-user-heat/1-boot-vm.yaml hw1
openstack stack list
openstack stack show hw1
watch openstack server list
openstack stack delete --wait --yes hw1
```

This simple template is enough to run a virtual machine.  However, it
is very static.  In the next subsection, we are going to manipulate
parameters to add flexibility.

## Need more flexibility: let's add parameters {#heat-params}

Templates can be more flexible with parameters.  To that end you can:
- Declare a set of parameters to provide to your template.
- Use the [intrinsic
  function](https://docs.openstack.org/heat/ussuri/template_guide/hot_spec.html#hot-spec-intrinsic-functions)
  `get_param` to map those parameters in your resource declarations.

The next template is an example with four parameters.  The first one
is related to the VM name and must be provided during the stack
creation.  The second one is the name of the VM image with a
`debian-10` as default value.  The third argument corresponds to the
flavor and defaults to `m1.small`.  Finally, the last one defines the
SSH key to use and defaults to `admin`.

{{ includecode(path="@/openstack-user-heat/2-boot-vm-with-params.yaml", syntax="yaml") }}

To deploy this stack, run the next command.  It deploys the VM by
overriding the default flavor value `m1.mini` with `m1.small`.  This
can be checked in `openstack server list`.

```bash
openstack stack create -t ./openstack-user-heat/2-boot-vm-with-params.yaml \
    --parameter the_vm_name=hello_params \
    --parameter the_flavor=m1.small \
    hw2
openstack server list
openstack stack delete --wait --yes hw2
```

The parameter `the_vm_name` is required since no default value is
provided.  If you try to create a stack without providing this
parameter, you end with an error.

```bash
$ openstack stack create -t ./openstack-user-heat/2-boot-vm-with-params.yaml \
    --parameter the_flavor=m1.medium \
    hw2_error

ERROR: The Parameter (the_vm_name) was not provided.
```

Parameters are the inputs of templates.  The next subsection focuses on
declaring outputs, so that a stack can return a set of attributes (e.g.,
the IP address of a deployed VM).

## Need to return values: let's use outputs {#heat-outputs}

Templates can declare a set of attributes to return.  For instance,
you might need to know the IP address of a resource at runtime.  To
that end, you can declare attributes in a new section called
`outputs`:

{{ includecode(path="@/openstack-user-heat/3-boot-vm-with-output.yaml", syntax="yaml") }}

The template declares an output attribute called `HOSTIP` which stores the
IP address of the VM resource.  To find the IP address, it uses another
[intrinsic
function](https://docs.openstack.org/heat/ussuri/template_guide/hot_spec.html#get-attr):
`get_attr`.  Same with the `HOSTNAME` output.  Output attributes can be
exploited in two ways: they can be displayed from the CLI, or they can be
fetched by other stack templates (we will see this last case latter):

```bash
openstack stack create -t ./openstack-user-heat/3-boot-vm-with-output.yaml hw3
openstack stack output list hw3
openstack stack output show hw3 HOSTIP
```

{% note() %}
Once again, the Heat documentation is your friend to find out
[attributes](https://docs.openstack.org/heat/ussuri/template_guide/openstack.html#OS::Nova::Server-attrs).  As such, you can reference the IP address with the
`network` attribute.

```yaml
get_attr: [heat-vm, networks, private, 0]
```

The source code of Heat also list [extra attributes](https://github.com/openstack/heat/blob/0703ca7bb19ca3bb06009c828a66bababf9970b8/heat/engine/resources/openstack/nova/server.py#L646-L736) that let you find
the IP address such as `first_address`, but that one is deprecated
though.

```yaml
get_attr: [heat-vm, first_address]
```

The Horizon dashboard has an "Orchestration" tab with a good list of
available functions and resources.

Finally, you can introspect all attributes of a resource with the
following command at runtime:

```bash
python -c "import pprint; pprint.pprint($(openstack stack resource show hw3 heat-vm -c attributes -f value))"
```

```python
{u'OS-DCF:diskConfig': u'MANUAL',
 # ...
 u'addresses': {u'private': [{u'OS-EXT-IPS-MAC:mac_addr': u'fa:16:3e:73:10:fe',
                           u'OS-EXT-IPS:type': u'fixed',
                           u'addr': u'192.168.222.84',
                           u'version': 4}]},
 # ...
 u'image': {u'id': u'3c91bbf5-5d1f-4e72-bf77-6dbc19c8351c',
            u'links': [{u'href': u'http://10.20.20.1:8774/images/3c91bbf5-5d1f-4e72-bf77-6dbc19c8351c',
                        u'rel': u'bookmark'}]},
 # ...
 u'name': u'hello_outputs'}
```
{% end %}

Remember to delete your stack at the end to release resources.

```bash
openstack stack delete --wait --yes hw3
```

## Integrate cloud-init

It is possible to declare a post-installation script in the template
with the `user_data` property.

{{ includecode(path="@/openstack-user-heat/4-boot-vm-with-user-data.yaml", syntax="yaml") }}

```bash
openstack stack create -t ./openstack-user-heat/4-boot-vm-with-user-data.yaml hw4
```

Associating a floating IP is a bit tricky with Heat, so let's do it
manually for now.  Then, wait for `cloud-init` to finish and finally,
SSH on the VM (the `wait_contextualization` function comes from
the [previous part](@/openstack-admin/cloud-init.md).

```bash
openstack server add floating ip hello_cloud_init \
    $(openstack floating ip create -c floating_ip_address -f value public)

wait_contextualization hello_cloud_init

openstack server ssh --login debian --identity ./admin.pem hello_cloud_init

openstack stack delete --wait --yes hw4
```

{% note() %}
Find the `user_data` file executed on the VM by cloud-init at
`/var/lib/heat-cfntools/cfn-userdata`.  This path comes from the log
of the VM boot (using `openstack console log show hello_cloud_init`)
right after the log `Cloud-init v. ... running`.
{% end %}

## Dynamic configuration with cloud-init and parameters

Let's mix parameters and cloud-init to write a template with a flexible
post-installation script.  With Heat, it is possible to provide a
parameter to your user-data at run-time by using a new [intrinsic
function](https://docs.openstack.org/heat/ussuri/template_guide/hot_spec.html#str-replace):
`str_replace`.

{{ includecode(path="@/openstack-user-heat/5-boot-vm-with-user-data2.yaml", syntax="yaml") }}

The template uses `str_replace` to instantiate variables in the
template.  In this example, the parameter should be a string
containing a set of packages to install in the VM. You can deploy the
stack as follow:

```bash
openstack stack create \
    -t ./openstack-user-heat/5-boot-vm-with-user-data2.yaml \
    --parameter package-names="vim cowsay fortune fortunes lolcat" \
   hw5
openstack stack delete --wait --yes hw5
```

This mechanism is crucial to dynamically configure our services during
the deployment.  For instance, `service-A` might require an IP address
in its configuration file to access `service-B`, which runs on another
VM.  This IP address is only known at run-time, so it must be
represented by a variable managed in Heat templates.  In the next
subsections, we are going to study how to declare such variable, so
that Heat resources can exchange information.
