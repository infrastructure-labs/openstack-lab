+++
title = "Advanced Heat"
weight = 4
+++

## Data dependency between resources {#data-deps}

Let's declare a template with two VMs: `user` and `provider`.  The
idea is to configure `user`'s static lookup table for hostnames (more
information can be found by typing: `man hosts`), so that user can
target `provider` from its hostname rather than its IP address.  To
that end, the template uses the `user_data` property together with the
`get_attr` function to edit the `/etc/hosts` file on `user`, and map
the IP address of `provider` with its hostname.

{{ includecode(path="@/openstack-user-heat/6-boot-vms-with-exchange.yaml", syntax="yaml") }}

In this example, `user` requires the IP address of `provider` to boot.
The Heat engine is in charge of managing dependencies between
resources.  Take a look during the deployment, and check that
`provider` is deployed prior to `user`.

```bash
openstack stack create -t ./openstack-user-heat/6-boot-vms-with-exchange.yaml hw6 \
  && watch openstack server list
openstack server add floating ip user \
  $(openstack floating ip create -c floating_ip_address -f value public)
openstack server ssh --login debian --identity ./admin.pem --address-type public user
debian@user:~$ ping provider -c 2
PING provider (192.168.222.238) 56(84) bytes of data.
64 bytes from provider (192.168.222.238): icmp_seq=1 ttl=64 time=1.27 ms
64 bytes from provider (192.168.222.238): icmp_seq=2 ttl=64 time=3.07 ms

debian@user:~$ exit
openstack stack delete --wait --yes hw6
```

## Nested templates

Heat is able to compose templates to keep human-readable files, using
nested templates.  For instance, we can use a first template that
describes a virtual machine, and a second template which deploys multiple
VMs by referencing the first one.  Rather than creating the first
template, we can re-use [a template from previous
examples](@/openstack-user-heat/discovering-heat.md#heat-params).

{{ includecode(path="@/openstack-user-heat/7-nested-templates.yaml", syntax="yaml") }}

To compose a template, a new resource can be defined by specifying its
type as the target of the desired template.  A set of properties can
be provided to the nested template and will be interpreted as
parameters.

```bash
openstack stack create -t ./openstack-user-heat/7-nested-templates.yaml hw7 \
  && watch openstack server list
openstack stack delete --wait --yes hw7
```

Nested templates are very convenient to keep your code clean and
re-usable.  Next section extends nested templates with data
dependency.

## Nested templates with data dependency

Let's describe the [same deployment as before](#data-deps) by using nested
templates. For that we need a new template:

{{ includecode(path="@/openstack-user-heat/8-nested-templates-boot-vm.yaml", syntax="yaml") }}

We can now declare the main template. While it defines three VMs, this
template is easy to read since it points to the template created just
above, as well as [a template from a previous
example](@/openstack-user-heat/discovering-heat.md#heat-outputs).

{{ includecode(path="@/openstack-user-heat/8-nested-templates-exchange.yaml", syntax="yaml") }}

```bash
openstack stack create -t ./openstack-user-heat/8-nested-templates-exchange.yaml hw8 \
  && watch openstack server list
openstack stack delete --wait --yes hw8
```

## Other type of resources: floating IP

It's Floating IP time!

{{ includecode(path="@/openstack-user-heat/9-floating-ip.yaml", syntax="yaml") }}

```bash
openstack stack create -t ./openstack-user-heat/9-floating-ip.yaml --wait hw9
```

You may find the floating IP by listing servers.

```bash
openstack server list
```

Or by asking Heat about attributes of the `floating-ip` resource.

```bash
FIP_RSC_ATTRIBUTES=$(openstack stack resource show -c attributes -f value hw9 floating-ip)
python -c "print('floating ip is %s' % ${FIP_RSC_ATTRIBUTES}['floating_ip_address'])"
```

Remember to delete your stack at the end to release resources.

```
openstack stack delete --wait --yes hw9
```
