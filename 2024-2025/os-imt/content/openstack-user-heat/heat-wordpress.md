+++
title = "Deploying Wordpress with Heat"
weight = 5
+++

As a DevOps at OWPH -- Online WordPress Hosting -- you are now in
charge of the automation process of deploying WordPress instances for
clients: Congratulation! To that end, you have to use what you learned
from the previous section to design a template that describes a
WordPress application using Heat. We are going to deploy WordPress
inside two VMs: the first one holds the web server, the second one
runs the database:

- VM1: Apache + PHP + WordPress code
- VM2: MariaDB

{% do() %}
Create three HOT files:

- `db-vm.yaml`: contains the description of the VM running MariaDB.
- `wp-vm.yaml`: contains the description of the VM running the Web server
  and serving WordPress.
- `wp-app.yaml`: contains the description of the WordPress application
  (glues the `db-vm.yaml` and `web-vm.yaml` together).

Once it is deployed, you should be able to reach the WordPress service by
going to `http://<wp-vm-fip-address>/wp`.
{% end %}
