+++
title = "Introduction to OpenStack"
weight = 1
sort_by = "weight"
insert_anchor_links = "right"
+++

OpenStack has become the de-facto solution to operate compute, network
and storage resources in public and private clouds.

In this lab, we are going to:
- Deploy an all-in-one OpenStack with [Snap microstack](https://snapcraft.io/microstack).
- Operate this OpenStack to manage IaaS resources (e.g., boot VMs,
  setup a private Network).
- Deploy a Wordpress as a Service.
- Start working on automation
