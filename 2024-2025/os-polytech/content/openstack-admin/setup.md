+++
title = "Initial setup"
weight = 1
+++

## Environment

OpenStack needs at least 6 GB of RAM to run. And plenty more to start VMs
on it.  For this lab, you will use the computers of the lab room of
Polytech, running with Ubuntu.

The lab makes use of [Snap
microstack](https://snapcraft.io/microstack): OpenStack in a
Snap that you can run locally on a single machine. Snap is Canonical's
App delivery mechanism. It enables developers to bundle all dependencies
into a single app package. And so does Snap microstack for an all-in-one
OpenStack on your machine.

An all-in-one OpenStack means that your machine will contain both services
to *operate* and *host* virtualized resources. For instance, the
`nova-conductor` to operate the boot of a VM, and `nova-compute` to host
the VM. This is a good setup for a lab, but not for production. There are
several other options such as
[DevStack](https://docs.openstack.org/devstack/latest/index.html),
[Puppet-OpenStack](https://docs.openstack.org/puppet-openstack-guide/latest/)
or [Kolla-ansible](https://docs.openstack.org/developer/kolla-ansible/)
and all matters. But, Snap microstack takes only 2 minutes to deploy
OpenStack (instead of 30 minutes for other options).

{% note() %}
- Devstack is good for OpenStack developers.
- Puppet-OpenStack or Kolla-ansible are good for production
  deployments.
{% end %}

## Resources of the lab {#lab-resources}

Get the resources of the lab at <https://infrastructure-labs.gitlabpages.inria.fr/openstack-lab/2024-2025/os-polytech/lab-os.tar.gz>

```bash
wget https://infrastructure-labs.gitlabpages.inria.fr/openstack-lab/2024-2025/os-polytech/lab-os.tar.gz
tar xzvf lab-os.tar.gz
cd openstack-admin/
```

The archive contains:
- **setup.sh**: script that sets up the lab.
- **teardown.sh**: script that uninstalls the lab.
- **other scripts**: other bash scripts useful for the lab.

## Setup OpenStack
On a recent Ubuntu, the `snap` package manager is already installed.

Install microstack using the `snap` package manager:
```bash
wget https://files.polyno.me/microstack_patched_pyroute2.snap
sudo snap install --devmode ./microstack_patched_pyroute2.snap
```

{% note() %}
Normally, we would simply install the official microstack package like this:

```bash
sudo snap install microstack --beta --devmode
```

However, the package is broken on Ubuntu 24.04 due to a [Linux kernel change](https://git.kernel.org/pub/scm/linux/kernel/git/netdev/net-next.git/commit/?id=a6cec0bcd34264be8887791594be793b3f12719f) that triggers [a bug in pyroute2](https://github.com/svinota/pyroute2/issues/923) that prevents [neutron-ovn-metadata-agent from working](https://bugs.launchpad.net/ubuntu/+source/neutron/+bug/1995735) (this breaks cloud-init).  While microstack is not updated with the fix, we rely on a manually patched version.
{% end %}

Execute the `setup.sh` file **as root** to initialize OpenStack (setup
networks, flavors, images, ...).
```bash
./setup.sh
```

{% note() %}
The script initializes all OpenStack services and databases, so it can
take a long time.  Don't be surprised if it takes 5 to 10 minutes.
However, if the script does not output anything new for several minutes,
it may be stuck; in that case, ask for help.
{% end %}

{% do() %}
Then, ensure OpenStack services are running on your machine. Find the
snap command that lists microstack OpenStack services and the
status? What is the purpose of each service?
{% end %}

{% solution() %}

```bash
snap services microstack | sort
```

- **`glance-*`**: Glance to manage VM images: `openstack image --help`.
- **`horizon-*`**: OpenStack Web dashboard: <https://localhost>.
- **`keystone-*`**: Keystone to manage authentication and authorization on
  OpenStack.
- **`neutron-*`**: Neutron to manage networks: `openstack network --help`.
- **`nova-*`**: Nova to manage VM: `openstack server --help`.
- **`memcached`**: Cache used by all OpenStack services
- **`mysqld`**: Database used by all OpenStack services
- **`rabbitmq-server`**: Communication bus used by all OpenStack services
{% end %}
