+++
title = "The Horizon dashboard"
weight = 2
+++

One service deployed is the OpenStack dashboard (Horizon). On your own
machine, horizon is reachable from the web browser at <https://localhost>
with the following credentials:
- login: `admin`
- password: `lab-os`

From here, you can reach `Project > Compute > Instances > Launch
Instance` and boot a virtual machine given the following information:
- a name (e.g., `horizon-vm`)
- an image (e.g., `cirros`) and set the `Create New Volume` to "No"
- a flavor to limit the resources of your instance (we recommend
  `m1.tiny`)
- and a network setting (must be `test`)

You should select options by clicking on the big arrow on the right of
each possibility. When the configuration is OK, the `Launch Instance`
button should be enabled. After clicking on it, you should see the
instance in the `Active` state in less than a minute.

Now, you have several options to connect to your freshly deployed VM.
For instance, after clicking on its name, Horizon provides a virtual
console under the `Console` tab. So, you can use the following
credentials to access the VM:
- login: `cirros`
- password: `gocubsgo`

However, as a *real DevOps*, you will prefer to access to your VM by
the command line interface...
