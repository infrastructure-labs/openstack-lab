+++
title = "Using SSH keys"
weight = 4
+++

Any cirros VMs share the same credentials (i.e., `cirros`, `gocubsgo`)
which is a security problem.  As a IaaS DevOps, you want that only
some clients can SSH on the VMs.  Fortunately, OpenStack helps with
the management of SSH keys.  OpenStack can generate a SSH key and push
the public counterpart on the VM.  Therefore, doing a `ssh` on the VM
will use the SSH key instead of asking the client to fill the
credentials.

Make an SSH key and store the private counterpart in `./admin.pem`.
Then, give that file the correct permission access.
```bash
openstack keypair create --private-key ./admin.pem admin
chmod 600 ./admin.pem
```

Start a new VM and ask OpenStack to copy the public counterpart of
your SSH key in the `~/.ssh/authorized_keys` of the VM (i.e., note the
`--key-name admin`).
```bash
openstack server create --wait --image cirros \
  --flavor m1.tiny --network test \
  --key-name admin cli-vm-with-ssh
```

Attach it a floating IP.
```bash
openstack server add floating ip \
  cli-vm-with-ssh \
  $(openstack floating ip create -c floating_ip_address -f value external)
```

Now you can access your VM using SSH without filling credentials.
```bash
openstack server ssh cli-vm-with-ssh \
  --login cirros \
  --identity ./admin.pem
```

{% note() %}
Or directly with the `ssh` command --- for bash lovers ❤.
```bash
ssh -i ./admin.pem -l cirros $(openstack server show cli-vm-with-ssh -c addresses -f value | sed  -Er 's/test=.+ (10\.20\.20\.[0-9]+).*/\1/g')
```

A regular `ssh` command looks like `ssh -i <identity-file> -l <name>
<server-ip>`. The OpenStack command followed by the `sed` returns the
floating IP of `cli-vm-with-ssh`. You may have to adapt it a bit
according to your network cidr.
```bash
openstack server show cli-vm-with-ssh -c addresses -f value \
    | sed -Er 's/test=.+ (10\.20\.20\.[0-9]+).*/\1/g'
```
{% end %}
