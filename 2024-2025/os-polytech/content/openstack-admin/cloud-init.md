+++
title = "cloud-init: VM contextualization"
weight = 5
+++

Contextualizing is the process that automatically installs software,
alters configurations, and does more on a machine as part of its boot
process.  On OpenStack, contextualizing is achieved thanks to
[`cloud-init`](https://cloud-init.io/).  It is a program that runs at the
boot time to customize the VM.

You have already used `cloud-init` without even knowing it!  The previous
command `openstack server create` with the `--identity` parameter tells
OpenStack to make the public counterpart of the SSH key available to the
VM.  When the VM boots for the first time, `cloud-init` is (among other
tasks) in charge of fetching this public SSH key from OpenStack, and copy
it to `~/.ssh/authorized_keys`.  Beyond that, `cloud-init` is in charge of
many aspects of the VM customization like mounting volume, resizing file
systems or setting an hostname (the list of `cloud-init` modules can be
found
[here](http://cloudinit.readthedocs.io/en/latest/topics/modules.html)).
Furthermore, `cloud-init` is able to run a bash script that will be
executed on the VM as `root` during the boot process.

## Debian FTW

When it comes the time to deal with real applications, we cannot use
cirros VMs anymore.  A Cirros VM is good for testing because it starts
fast and has a small memory footprint.  However, do not expect to
launch [MariaDB](https://en.wikipedia.org/wiki/MariaDB) on a cirros.

We are going to run several Debian VMs in this section. However, a Debian
VM takes a lot more of resources to run.  For this reason, you may want to
release all your resources before going further.

```bash
# Delete VMs
for vm in $(openstack server list -c ID -f value); do \
  echo "Deleting ${vm}..."; \
  openstack server delete "${vm}"; \
done

# Releasing floating IPs
for ip in $(openstack floating ip list -c "Floating IP Address" -f value); do \
  echo "Releasing ${ip}..."; \
  openstack floating ip delete "${ip}"; \
done
```

{% do() %}
Go to <https://cloud.debian.org/cdimage/cloud/> and find the appropriate
Debian image to download.  You should use the latest Debian stable version
and the qcow2 format.
{% end %}

{% solution() %}
The image must support cloud-init (i.e. have the `cloud-init` software
pre-installed).  We use the `genericcloud` variant because the image is
smaller.

```bash
wget -O debian-12.qcow2 \
    https://cloud.debian.org/cdimage/cloud/bookworm/latest/debian-12-genericcloud-amd64.qcow2
```
{% end %}

{% do() %}
Import the image into Glance; name it `debian-12`. Use `openstack image
create --help` for creation arguments.  Find values example with
`openstack image show cirros`.
{% end %}

{% solution() %}

```bash
openstack image create --disk-format=qcow2 \
  --container-format=bare --property architecture=x86_64 \
  --public --file ./debian-12.qcow2 \
  debian-12
```
{% end %}

{% do() %}
Now create a new `m1.mini` flavor with 5 GB of Disk, 2 GB of RAM, 1
vCPU and 1 GB of swap.  Use `openstack flavor create --help` for
creation arguments.
{% end %}

{% solution() %}

```bash
openstack flavor create --ram 2048 \
  --disk 5 --vcpus 1 --swap 1024 \
  --public m1.mini
```
{% end %}

## cloud-init in action

To tell `cloud-init` to load and execute a specific script at boot
time, you should append the `--user-data <file/path/of/your/script>`
extra argument to the regular `openstack server create` command.

{% do() %}
Start a new VM named `art-vm` based on the `debian-12` image
and the `m1.mini` flavor. The VM should load and execute the script
`openstack-admin/art.sh` that installs the
[figlet](https://github.com/cmatsuoka/figlet) and
[lolcat](https://github.com/busyloop/lolcat) software on the VM.
{% end %}

{% solution() %}

```bash
openstack server create --wait --image debian-12 \
  --flavor m1.mini --network test \
  --key-name admin \
  --user-data ./openstack-admin/art.sh \
  art-vm
```
{% end %}

You can follow the correct installation of software with:

```bash
watch openstack console log show --lines=20 art-vm
```

{% do() %}
Could you notice *when* the VM has finished to boot based on the
`console log` output?  Write a small bash script that waits until the
boot has finished.
{% end %}

{% solution() %}

```bash
function wait_contextualization {
  # VM to get the log of
  local vm="$1"
  # Number of rows displayed by the term
  local term_lines=$(tput lines)
  # Number of log lines to display is min(term_lines, 20)
  local console_lines=$(($term_lines<22 ? $term_lines - 2 : 20))
  # Get the log
  local console_log=$(openstack console log show --lines=${console_lines} "${vm}")

  # Do not wrap long lines
  tput rmam

  # Loop till cloud-init finished
  local cloudinit_end_rx="Cloud-init v\. .\+ finished"
  echo "Waiting for cloud-init to finish..."
  echo "Current status is:"
  while ! echo "${console_log}"|grep -q "${cloudinit_end_rx}"
  do
      echo "${console_log}"
      sleep 5

      # Compute the new console log before clearing
      # the screen is it does not remain blank for two long.
      local new_console_log=$(openstack console log show --lines=${console_lines} "${vm}")

      # Clear the screen (`cuu1` move cursor up by one line, `el`
      # clear the line)
      while read -r line; do
          tput cuu1; tput el
      done <<< "${console_log}"

      console_log="${new_console_log}"
  done

  # cloud-init finished
  echo "${console_log}"|grep --color=always "${cloudinit_end_rx}"

  # Re-enable wrap of long lines
  tput smam
}
```

Then use it as the following:
```bash
wait_contextualization art-vm
```
{% end %}

Then, attach it a floating IP.
```bash
openstack server add floating ip \
  art-vm \
  $(openstack floating ip create -c floating_ip_address -f value external)
```

Hence, you can jump on the VM and call the `figlet` and `lolcat`
software:
```bash
openstack server ssh art-vm \
    --login debian \
    --identity ./admin.pem

The authenticity of host '10.20.20.13 (10.20.20.13)' can't be established.
ECDSA key fingerprint is SHA256:WgAn+/gWYg9MkauihPyQGwC0LJ8sLWM/ySrUzN8cK9w.
Are you sure you want to continue connecting (yes/no)? yes
Failed to add the host to the list of known hosts (/root/.ssh/known_hosts).
```

```bash
debian@art-vm:~$ figlet "The Art of Contextualizing a VM" | lolcat
```
