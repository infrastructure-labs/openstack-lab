#!/usr/bin/env bash

function wait_contextualization {
  # VM to get the log of
  local vm="$1"
  # Number of rows displayed by the term
  local term_lines=$(tput lines)
  # Number of log lines to display is min(term_lines, 20)
  local console_lines=$(($term_lines<22 ? $term_lines - 2 : 20))
  # Get the log
  local console_log=$(openstack console log show --lines=${console_lines} "${vm}")

  # Do not wrap long lines
  tput rmam

  # Loop till cloud-init finished
  local cloudinit_end_rx="Cloud-init v\. .\+ finished"
  echo "Waiting for cloud-init to finish..."
  echo "Current status is:"
  while ! echo "${console_log}"|grep -q "${cloudinit_end_rx}"
  do
      echo "${console_log}"
      sleep 5

      # Compute the new console log before clearing
      # the screen is it does not remain blank for two long.
      local new_console_log=$(openstack console log show --lines=${console_lines} "${vm}")

      # Clear the screen (`cuu1` move cursor up by one line, `el`
      # clear the line)
      while read -r line; do
          tput cuu1; tput el
      done <<< "${console_log}"

      console_log="${new_console_log}"
  done

  # cloud-init finished
  echo "${console_log}"|grep --color=always "${cloudinit_end_rx}"

  # Re-enable wrap of long lines
  tput smam
}

openstack server create --wait --image debian-12 \
  --flavor m1.mini --network test \
  --key-name admin \
  --user-data ./openstack-admin/install-mariadb.sh \
  wordpress-db

wait_contextualization wordpress-db

# Set the script with IP address of `wordpress-db`.
sed -i '13s|.*|DB_HOST="'$(openstack server show wordpress-db -c addresses -f value | sed -Er "s/test=//g")'"|' ./openstack-admin/install-wordpress.sh

# Then, create `wordpress-app`.
openstack server create --wait --image debian-12 \
  --flavor m1.mini --network test \
  --key-name admin \
  --user-data ./openstack-admin/install-wordpress.sh \
  wordpress-app

wait_contextualization wordpress-app

# Get a floating ip for the VM
WP_APP_FIP=$(openstack floating ip create -c floating_ip_address -f value external)

# Attach the `WP_APP_FIP` floating ip to that VM.
openstack server add floating ip wordpress-app "${WP_APP_FIP}"
