+++
title = "Creating your first VM through the CLI"
weight = 3
+++

While Horizon is helpful to discover OpenStack features, this is not
the tool of choice for an operator.  An operator prefers command line
interface 😄.  You are lucky, OpenStack provides one.

All operations to manage OpenStack are done through one unique command
line, called `openstack <service> <action> ...`.  Doing an `openstack
--help` displays the *really long* list of services/possibilities
provided by this command.  The following gives you a selection of the
most often used commands to operate your Cloud:

- List OpenStack running services:

```bash
openstack endpoint list
```

- List images:

```bash
openstack image list
```

- List flavors:

```bash
openstack flavor list
```

- List networks:

```bash
openstack network list
```

- List computes:

```bash
openstack hypervisor list
```

- List VMs (running or not):

```bash
openstack server list
```

- Get details on a specific VM:

```bash
openstack server show <vm-name>
```

- Start a new VM:

```bash
openstack server create --image <image-name> \
    --flavor <flavor-name> --network <network-name> \
    <vm-name>
```

- View VMs logs:

```bash
openstack console log show <vm-name>
```

{% do() %}
Try one of these commands.  Does it works?  What is the problem, how to
fix it?  Hint: Look at the [password authentication
process](https://docs.openstack.org/python-openstackclient/ussuri/cli/authentication.html)
for the CLI. Second hint: After you saw how cumbersome it is to add the
credentials to each command, you can find how to source them thanks to the
dashboard (see
<https://docs.openstack.org/liberty/install-guide-obs/keystone-openrc.html>).
{% end %}

{% solution() %}

```bash
$ openstack endpoint list
Missing value auth-url required for auth plugin password

Similarly to Horizon, you have to provide your credentials to the
OpenStack CLI and tell it the URL of the authentication service.
There are **two options** to achieve this.  First, to give them as
arguments of the command.

```bash
openstack server list --os-auth-url=https://localhost:5000/v3/ \
                        --os-username=admin \
                        --os-password=lab-os \
                        --os-project-name=admin \
                        --os-user-domain-name=Default \
                        --os-project-domain-id=default
```

This is a bit cumbersome since you have to give them every time.  The
second option consists in seting your credentials as variables in your
bash [environment](https://www.gnu.org/software/coreutils/manual/html_node/env-invocation.html#env-invocation).  Hence, the CLI automatically reads these variables
instead.  You can find a pre-generated file with all variables
properly set under the Horizon interface by clicking on the `admin`
dropdown list at the top right corner, and get the "OpenStack RC
File".

To setup your environment, download this file on your Lab machine and
source it.
```bash
source ./admin-openrc.sh
```

You can then check that your environment is correctly set.
```bash
$ env|fgrep OS_|sort

OS_AUTH_URL=http://localhost:5000/v3/
OS_IDENTITY_API_VERSION=3
OS_INTERFACE=public
OS_PASSWORD=lab-os
OS_PROJECT_DOMAIN_ID=default
OS_PROJECT_ID=2bad71b9246a4a06a0c9daf2d8896108
OS_PROJECT_NAME=admin
OS_REGION_NAME=microstack
OS_USER_DOMAIN_NAME=Default
OS_USERNAME=admin
```
{% end %}

{% do() %}
Using all these commands, use the CLI to start a new tiny cirros VM
called `cli-vm`.
{% end %}

{% solution() %}

```bash
openstack server create \
  --image cirros \
  --flavor m1.tiny \
  --network test \
  cli-vm
```
{% end %}

Then, display the information about your VM with the following
command:
```bash
openstack server show cli-vm
```

Note in particular the `status` of your VM.
```bash
openstack server show cli-vm -c status -f json
```

This status will go from `BUILD`: OpenStack is looking for the best
place to boot the VM; to `ACTIVE`: your VM is running.  The status
could also be `ERROR` if you are experiencing hard times with your
infrastructure.

{% do() %}
What is the purpose of the `-c` and `-f` argument in the previous
command.
{% end %}

{% solution() %}

```bash
$ openstack server create --help | fgrep -A 6 "output formatters:"
output formatters:
  output formatter options

  -f {json,shell,table,value,yaml}, --format {json,shell,table,value,yaml}
                        the output format, defaults to table
  -c COLUMN, --column COLUMN
                        specify the column(s) to include, can be repeated
```
{% end %}

A VM in `ACTIVE` state still has to go through the [boot process and
init](http://www.tldp.org/LDP/intro-linux/html/sect_04_02.html). Hence,
you may still have to wait for one minute or two that your VM finishes to
boot. You can check that your VM finished to boot by looking at its logs
with `openstack console log show cli-vm`. A CirrOS VM finished to boot
when last lines are:

```
=== cirros: current=0.4.0 latest=0.4.0 uptime=29.16 ===
  ____               ____  ____
 / __/ __ ____ ____ / __ \/ __/
/ /__ / // __// __// /_/ /\ \
\___//_//_/  /_/   \____/___/
   http://cirros-cloud.net


login as 'cirros' user. default password: 'gocubsgo'. use 'sudo' for root.
cli-vm login:
```

## Make the world reach the VM

The [neutron](https://docs.openstack.org/neutron/ussuri/) service manage
networks in OpenStack.  Neutron distinguishes, at least two kind of
networks.  First, the *project (or tenant) network* to provide
communication between VMs of the same project.  Second, the *provider (or
external) network* to provide an access to the VM from the outside.  With
the previous `openstack server create` command, the VM boots with an IP on
the tenant network.  Consequently, you cannot ping your VM from an
external network (e.g., the Lab machine).

{% do() %}
Find the IP address of the `cli-vm`. Check that you can ping that
address from the `horizon-vm` (using the `Console` tab in the Horizon
dashboard).  Ensure that you **cannot** ping that VM from the Lab machine.
{% end %}

{% solution() %}

```bash
PRIV_IP=$(openstack server show cli-vm -c addresses -f value | sed -E 's/test=(.+)/\1/g')
echo "Private IP of cli-vm is ${PRIV_IP}"
ping -c 3 "${PRIV_IP}" # From horizon-vm: 0% packet loss, From lab: 100% packet loss
```
{% end %}

To ping your VM from the Lab machine, you have to affect it an IP
address of the `external` network.  The management of the external
network is done typically at the level of the infrastructure and not
by OpenStack.  OpenStack allows to access IP addresses of that network
using *floating IPs*.  A floating IP is not allocated to a specific VM
by default. Rather, an operator has to explicitly *pick* one from a
pool and then attach it to its VM. Thus, if the VM dies for some
reason, the operator does not lose the floating IP -- it remains her
own resource, ready to be attached to another VM.  For instance, OVH
uses that mechanism to assign public IP addresses to VMs.

{% do() %}
Using the CLI, find out the IP address range used by the `external` network.
Hint: try to look at OpenStack objects of type `network` and `subnet`.
{% end %}

{% solution() %}

```bash
openstack network list
openstack subnet list
openstack subnet show external-subnet
```
{% end %}

Allocate and attach a floating IP from the `external` network to your machine if you
want it to be reachable from the host:
```bash
# Allocate an IP on the "external network"
openstack floating ip create external
# Look at the result and identify the IP address
openstack floating ip list
# Now "attach" it to the VM
openstack server add floating ip cli-vm "YOUR_FLOATING_IP_ADDRESS"
```

Then, ask again for the status of your VM and its IPs.
```bash
openstack server show cli-vm -c status -c addresses
```

{% do() %}
Ping `cli-vm` on its floating IP.
```bash
ping -c 3 "YOUR_FLOATING_IP_ADDRESS"
```

Does it work? Why? Hint: OpenStack limits the traffic for security
reasons.  The mechanisms to control the traffic in OpenStack is called
[security
group](https://docs.openstack.org/neutron/ussuri/feature_classification/general_feature_support_matrix.html#operation_Security_Groups).
Find the command that list the security group rules of the `admin`
project.
{% end %}

{% solution() %}

```bash
$ SECGROUP_ID=$(openstack security group list --project admin -f value -c ID)
$ openstack security group rule list --long -c "IP Protocol" -c "IP Range" -c Direction $SECGROUP_ID

+-------------+------------------+-----------+
| IP Protocol | IP Range         | Direction |
+-------------+------------------+-----------+
| None        | 192.168.222.0/24 | ingress   |
| None        | 0.0.0.0/0        | egress    |
+-------------+------------------+-----------+
```

By default, OpenStack is very conservative and only allows two kinds
of intercommunication patterns:
1. Any intercommunication among hosts of the same project.  This is
   the first line.  It should be read as "*Neutron allows incoming
   traffic (`ingress`) between* *hosts of `192.162.222.*` of any
   protocol (`None` specific ones)*".
2. Any kind of outgoing communications.  This is the second line.  It
   should be read as "*Neutron allows outgoing traffic (`egress`) to
   anywhere (`0.0.0.0/0`) and of any protocol (`None`)*".
And that's it.  Since there are no more rules, it means that OpenStack
prevents all other ingress communications including communications on
`10.20.20.*`.

Commonly, OpenStack states the first intercommunication pattern of
"allowing traffic among hosts of the same project" not as we see it
here, but using a *remote security group*.  While specifying a
security group rule, the DevOps gives either an IP Range (e.g.,
`192.168.222.0/24`) with `--remote-ip`, or machines that belongs to a
specific group with `--remote-group`.  Using the latter, OpenStack
implements the first intercommunication pattern with a rule that tells
Neutron to allow traffic between hosts of the group `$SECGROUP_ID`.

```bash
$ openstack security group rule create $SECGROUP_ID --remote-group $SECGROUP_ID
+-------------+------------------+-----------+
| IP Protocol | Remote Group     | Direction |
+-------------+------------------+-----------+
| None        | <SECGROUP_ID>    | ingress   |
+-------------+------------------+-----------+
```

{% end %}

{% do() %}
Then, make it works for `10.20.20.0/24` network.  See examples of
security groups rules in the [neutron doc](https://docs.openstack.org/neutron/ussuri/admin/deploy-lb-selfservice.html#verify-network-operation).
{% end %}

{% solution() %}
To make it works, you have to setup new rules in the security group of
the `admin` project. The following rules allow ICMP packets (for ping)
and TCP on port 22 (for SSH connection) on the VM.

```bash
openstack security group rule create $SECGROUP_ID --proto icmp \
    --remote-ip 10.20.20.0/24
openstack security group rule create $SECGROUP_ID --proto tcp \
    --remote-ip 10.20.20.0/24 --dst-port 22
```
{% end %}

Once you succeed to ping the vm, you should also be able to SSH on it.

```bash
ssh -l cirros "$ALLOCATED_FIP"
```

{% do() %}
Go on, and play with the `openstack` CLI.  List all features offered
by Nova with `openstack server --help` and figure out how to:
1. SSH on `cli-vm` using its name rather than its IP;
2. Pause it, note the footprint on the ram of the hypervisor, and unpause it;
3. ~~Suspend it, note the footprint on the ram of the hypervisor, and
   resume it;~~ Does not work right now 😒.
4. Create a snapshot of `cli-vm`;
5. Boot a new machine `cli-vm-clone` from the snapshot;
6. Delete `cli-vm-clone`;
{% end %}

{% solution() %}
```bash
# 1.
openstack server ssh cli-vm -l cirros
# 2.
CLI_VM_HYPERVISOR=$(openstack server show cli-vm -c "OS-EXT-SRV-ATTR:hypervisor_hostname" -f value)
openstack hypervisor show -c free_ram_mb "$CLI_VM_HYPERVISOR"
openstack server pause cli-vm; openstack server show cli-vm -c status
openstack hypervisor show -c free_ram_mb "$CLI_VM_HYPERVISOR"
openstack server unpause cli-vm; openstack server show cli-vm -c status
# 3.
openstack server suspend cli-vm; openstack server show cli-vm -c status
openstack hypervisor show -c free_ram_mb "$CLI_VM_HYPERVISOR"
openstack server resume cli-vm; openstack server show cli-vm -c status
# 4.
openstack server image create --name cli-vm-img cli-vm; openstack image list
# 5.
openstack server create --wait --flavor m1.tiny \
  --network test --image cli-vm-img \
  cli-vm-clone
# 6.
openstack server delete cli-vm-clone
```
{% end %}
