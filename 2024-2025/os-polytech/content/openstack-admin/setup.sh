#!/usr/bin/env bash

set -o errexit
set -o xtrace

# Set the admin password
snap set microstack config.credentials.keystone-password=lab-os
# Automatically detect if KVM is usable
snap set microstack config.host.check-qemu=true
# Enable IP forward to provide network connectivity to VMs
snap set microstack config.host.ip-forwarding=true

# For some reason, ovs-vswitchd fails to load this kernel module itself
# (on Ubuntu 24.04)
modprobe openvswitch

# Initialize  OpenStack
microstack.init --auto --control

# Fix certificate verification
cp /var/snap/microstack/common/etc/ssl/certs/cacert.pem /usr/local/share/ca-certificates/microstack.crt
update-ca-certificates

# Install openstack client
snap info openstackclients | fgrep -q installed && sudo snap remove --purge openstackclients
snap install openstackclients --channel=ussuri/stable

set +o xtrace

# Remove icmp and tcp security group rules of `microstack.init --auto`
for rule in $(microstack.openstack security group rule list --protocol icmp -c ID -f value)
do
    microstack.openstack security group rule delete "${rule}"
done
for rule in $(microstack.openstack security group rule list --protocol tcp -c ID -f value)
do
    microstack.openstack security group rule delete "${rule}"
done

set -o xtrace
