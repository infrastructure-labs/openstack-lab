# OpenStack lab

This repository contains course material to run an OpenStack lab.

## Pedagogical goals

The goal of the lab is to demonstrate the use of OpenStack through experiments
with a real OpenStack system.  Students will:

- understand how OpenStack services are organized
- learn to use Openstack through the web-based dashboard (Horizon)
- learn to use Openstack through the CLI
- become familiar with core concepts such as VMs, networks, floating IPs, security groups, cloud-init
- be introduced to automation and orchestration with Heat templates

## Target audience

The lab is targeted at Master 2 students, or equivalently in their third year of engineering school.
They should already be proficient with basic system and networking concepts such as: SSH, virtualisation,
IP addresses.

## Known courses using this lab

- [formation ingénieur par apprentissage, spécialité ingénierie logicielle (FIL A3)](https://www.imt-atlantique.fr/fr/formation/ingenieur-par-apprentissage/ingenierie-logicielle), IMT Atlantique, campus de Nantes (module Cloud)
- [formation ingénieur généraliste, spécialité ingénierie logicielle et innovation (TAF LOGIN A3)](https://www.imt-atlantique.fr/fr/formation/ingenieur-generaliste?p=Y%3DtMdbkicc503AUGbZ%3DRvDAMNtX31MjyN1TU1NTtJOHwLt2T#0ZHTcJ3Ov1My), IMT Atlantique, campus de Nantes
- [formation ingénieur Polytech Nantes (INFO 5)](https://polytech.univ-nantes.fr/fr/les-formations/cycle-ingenieur/ingenieur-informatique) (module Réseaux-Système-Cloud)
- [formation ingénieur Systèmes réseaux et télécommunications par apprentissage (SRT5)](https://polytech.univ-nantes.fr/fr/les-formations/cycle-ingenieur/srt), Polytech Nantes, La Roche-sur-Yon
- [Master Informatique parcours Architecture Logicielle (ALMA)](https://sciences-techniques.univ-nantes.fr/formations/masters/master-informatique), Nantes Université

## Links to specific course materials

The material content is usually adapted to each course and can be slightly different, according to
relevant constraints (duration of the lab, local IT environment).  In addition, the material is
improved every year according to feedback from students.

- [2024-2025 Polytech Nantes INFO5](https://infrastructure-labs.gitlabpages.inria.fr/openstack-lab/2024-2025/os-polytech/intro/) (solutions available to students but hidden by default)
  - same subject as SRT5
- [2024-2025 IMT Atlantique](https://infrastructure-labs.gitlabpages.inria.fr/openstack-lab/2024-2025/os-imt/intro/) (solutions available to students but hidden by default)
  - Microstack section based on ALMA 2024-2025
  - Heat section based on IMT Atlantique 2023-2024
  - update OpenStack documentation links
- [2024-2025 Nantes Université ALMA](https://infrastructure-labs.gitlabpages.inria.fr/openstack-lab/2024-2025/os-nu-alma/intro/) (solutions available to students but hidden by default)
  - based on IMT Atlantique lab subject, but without the section on Heat
  - rework section on security groups: use the default security groups to avoid confusion
  - run lab on Grid'5000, using Ubuntu 22.04 to keep compatibility with microstack
  - 34 students (each with a dedicated Grid'5000 node)
- [2024-2025 Polytech Nantes SRT5](https://infrastructure-labs.gitlabpages.inria.fr/openstack-lab/2024-2025/os-polytech/intro/) (solutions available to students but hidden by default)
  - last-minute workaround for Ubuntu 24.04 (the version of microstack we use is broken on newer kernels)
  - 14 students (on 12 lab computers)
- [2023-2024 Polytech Nantes](https://infrastructure-labs.gitlabpages.inria.fr/openstack-lab/2023-2024/os-polytech/intro/) (solutions available to students but hidden by default)
  - remove section on Heat (never done at Polytech)
  - add more step-by-step questions to learn the CLI
  - adapt for local context (no need for iptables redirect, etc)
  - update VMs from Debian 10 to Debian 12
  - setup.sh: don't install any APT packages, so that it works on older Ubuntu with misconfigured sources.list
  - 15 students (on 14 lab computers)
- [2023-2024 IMT Atlantique](https://infrastructure-labs.gitlabpages.inria.fr/openstack-lab/2023-2024/os-imt/intro/) (solutions available to students but hidden by default)
  - convert everything from org-mode to [Zola](https://www.getzola.org/), with automatic CI build/publish
  - reorganize content in two main sections: first Microstack, then Heat
  - remove subsection on qemu/KVM configuration, it was confusing and not very helpful
  - fix long-standing typo when enabling IP forwarding in Microstack
  - update OpenStack documentation links
- [2022-2023 La Roche-sur-Yon SRT5](https://infrastructure-labs.gitlabpages.inria.fr/openstack-lab/2022-2023/os-polytech-laroche/) (with solutions shown)
- [2022-2023 IMT Atlantique](https://infrastructure-labs.gitlabpages.inria.fr/openstack-lab/2022-2023/os-imt/) (with solutions shown)
  - move to new Gitlab repository <https://gitlab.inria.fr/infrastructure-labs/openstack-lab> with Gitlab-CI
  - update Enoslib-based deployment script for microstack
  - rework and fix Enos-based deployment scripts for Heat
  - setup.sh: fix certificate error when using the CLI
- [2021-2022 Polytech Nantes](https://marie-donnie.github.io/teachings/2021-2022/os-polytech/index.html) (with solutions shown)
- [2021-2022 IMT Atlantique](https://marie-donnie.github.io/teachings/2021-2022/os-imt/index.html) (with solutions shown)
- [2020-2021 Polytech Nantes](https://rcherrueau.github.io/teaching/2020-2021/os-polytech/) (with solutions shown)
- [2020-2021 IMT Atlantique](https://rcherrueau.github.io/teaching/2020-2021/os-imt/) (with solutions shown)
- [2019-2020 IMT Atlantique](https://rcherrueau.github.io/teaching/2019-2020/os-imt/) (with solutions shown)
- [2019-2020 Polytech Nantes](https://rcherrueau.github.io/teaching/2019/os-polytech-nantes/) (with solutions shown)
- [2019-2020 La Roche-sur-Yon SRT5](https://rcherrueau.github.io/teaching/2019/os-polytech-laroche/) (with solutions shown)
- [2018-2019 IMT Atlantique](https://rcherrueau.github.io/teaching/2019/os-imt/) (with solutions shown)
- [2018-2019 Polytech Nantes](https://rcherrueau.github.io/teaching/ptech18/) (with solutions shown)

## Using Zola to build the content

For local development:

- [Install Zola](https://www.getzola.org/documentation/getting-started/installation/).  I tend to use the version from Snap, or the Docker image.
- Go to a specific subject, e.g. `2023-2024/os-polytech`
- Run `zola serve`
- If there are no errors, the course is accessible locally on <http://127.0.0.1:1111>
- Zola will automatically rebuild the content when you change something

When you push, Gitlab CI will automatically rebuild the site and upload it to <https://infrastructure-labs.gitlabpages.inria.fr/openstack-lab/SUBDIRECTORIES/>.

You might want to run `zola check` from time to time to check the validity of external links.

## How labs are run

The entrypoint of the lab is a shared document that you have to create.  We use <https://notes.inria.fr>, set
the access to "freely", and generate a short link with <https://s.42l.fr/>.  We give the short link to students.

On this shared document, we put all information necessary for students during the lab:

- URL to the lab material
- IP address of each student's machine (when using Grid'5000 for the first part)
- logins and passwords that students can use to connect
- URL of the shared Horizon dashboard for the second part

You can ask students to indicate in this shared document when they have reached a certain point
(e.g. checkboxes, or simply ask them to write "OK" beside their IP address when they successfully connected to the machine).
This can help you identify students that are stuck.

### First part with microstack

**Duration: 6 hours** (on the same day, for instance 2x 3h)

The first part is done with [Microstack](https://microstack.run/) on Ubuntu.
It means that each student runs its own dedicated OpenStack on a single machine.
Each student has an admin role on their OpenStack.

The minimal configuration required for the lab is 2 CPU cores, 8 GB RAM, 15 GB local disk space.
The lab works both on Ubuntu 20.04 and 22.04.  The course material mostly assumes Ubuntu 22.04 running on a physical node.
If your Ubuntu is a VM, you would need nested virtualisation (we have not fully tested this approach).

**Warning:** the lab does not work properly on Ubuntu 24.04: it might appear to work at first,
but VMs will not obtain their SSH key or user-data script through cloud-init, and we rely heavily on cloud-init in the lab.
This is caused by a [Linux kernel change](https://git.kernel.org/pub/scm/linux/kernel/git/netdev/net-next.git/commit/?id=a6cec0bcd34264be8887791594be793b3f12719f)
that triggers [a bug in pyroute2](https://github.com/svinota/pyroute2/issues/923)
that prevents [neutron-ovn-metadata-agent from working](https://bugs.launchpad.net/ubuntu/+source/neutron/+bug/1995735)
and this breaks cloud-init.  The [microstack package we use](https://snapcraft.io/microstack) seems unmaintained,
so there is little chance that it will be fixed.  Canonical has published a new independent
[snap package called "openstack"](https://snapcraft.io/openstack) that runs with a different technical stack.
Future version of this lab will switch to this new snap package.

Depending on the teaching environment, we either use local machines or Grid'5000:

#### Option 1: local machines

Using microstack on local machines has three main requirements:

1. root access on local machines (to install microstack with snap, and a few needed APT packages)
2. unfiltered access to the Internet (to install microstack and APT packages, to use a third-party DNS resolver in VMs)
3. good homogeneity across students' machines (to make sure OpenStack works as expected on all machines)

If you don't have homogeneity, you will quickly get into trouble and might spend all the lab
trying to fix things on each student's local machine!  We've seen issues such as inconsistent proxy or APT configuration,
an existing mysql service on the host preventing the snap-managed mysql service to start,
iptables policy issues with an existing Docker installation (`FORWARD DROP` on the host preventing VMs to use the network),
non-working VMs because of network filtering on the university network (outgoing DNS packet filtering, mandatory use of HTTP proxy).

Even worse: forget about running this lab on students' laptops directly, it will simply not work.

All in all, this approach works well in universities that have dedicated lab rooms with computers for networking lab,
because these labs have similar requirements (Linux, root access, no filtering).  We do this at Polytech Nantes.
You still need to make sure that all machines have reasonably similar configuration.  One easy way is to ask local IT staff
to reinstall the machines before your lab, they usually have automated tools to do that.

At the end of the lab, don't forget to tell students to run the `teardown.sh` script to uninstall everything and
(hopefully) put the machines back into a good state.

#### Option 2: Grid'5000

When we can't use local machines, we use [Grid'5000](https://www.grid5000.fr/w/Grid5000:Home), which is much more
comfortable.

Students don't reserve resources themselves, but they still need a Grid'5000 account to connect to the Grid'5000 VPN.
You need to use the [Grid'5000 teaching lab how-to](https://www.grid5000.fr/w/Tutorial_or_Teaching_Labs_HowTo) to create
account for students in advance.

We reserve one physical machine on Grid'5000 for each student, pre-deploy Ubuntu, and give students the root password.
This is done with a script based on [Enoslib](https://discovery.gitlabpages.inria.fr/enoslib/), stored under `setup-infra/`
in this git repository.  Example usage:

- edit the script (change the reservation date, project name, Grid'5000 cluster, students list)
- copy the script to a Grid'5000 frontend (with scp)
- connect through SSH to the Grid'5000 frontend
- create a virtualenv and install dependencies:

```
$ cd setup-infra/microstack
$ python3 -m venv venv-microstack
$ . venv-microstack/bin/activate
$ pip install -r requirements.txt
```

- run the script:

```
$ ./setup.py
```

Alternatively, you can run the script locally on your machine.  In that case you need to use the [Grid'5000 VPN](https://www.grid5000.fr/w/VPN)
and to setup [Grid'5000 API configuration](https://discovery.gitlabpages.inria.fr/enoslib/tutorials/grid5000.html#configuration).

### Second part (Heat)

**Duration: 4 hours** (not necessarily the same day as the first part)

The second part on Heat cannot be done with Microstack, because the Heat services are not installed as part of the Microstack package.

We take this opportunity to setup a more realistic OpenStack cloud, with several compute nodes, working external subnets for floating IPs, etc.
The deployment is done with [EnOS](https://beyondtheclouds.github.io/enos/) on [Grid'5000](https://www.grid5000.fr/w/Grid5000:Home).

#### Step 1: deploy OpenStack with EnOS

This is the most delicate step: OpenStack is very complex and things might break.
You should test it in advance and run the lab to make sure everything works.
Also, keep in mind that an EnOS deployment can take 30 minutes even on fast servers.

- either copy the scripts on a Grid'5000 frontend, or use the [Grid'5000 VPN](https://www.grid5000.fr/w/VPN)
  and setup [Grid'5000 API configuration](https://discovery.gitlabpages.inria.fr/enoslib/tutorials/grid5000.html#configuration).
- setup virtualenv and install dependencies:

```
$ cd setup-infra/heat
$ python3 -m venv venv-heat
$ . venv-heat/bin/activate
$ pip install -r requirements.txt
```

- edit the existing `reservation-XXX.yaml` config (walltime, project name, cluster, number of nodes) or generate a new one with `enos new`
- run EnOS:

```
$ time enos -v deploy -f reservation-XXX.yaml
```

This should complete in around 30 minutes, depending on the hardware and network conditions.

#### Step 2: create OpenStack users and projects for students

- note the DNS name or IP address of the OpenStack control plane.  It is shown at the end of the output of `enos deploy`
- verify you can connect the Horizon web dashboard
- edit the `setup-XXX.py` script (DNS resolver, control plane name/IP, list of students)
- run it in the same virtualenv:

```
$ . venv-heat/bin/activate
$ ./setup-XXX.py
```

Each student should now be able to login on the Horizon dashboard with a separate account.

## Authors and license

Authors: Ronan-Alexandre Cherrueau (original material, org-mode), Adrien Lèbre (original material), Marie Delavergne (improvements and updates), Baptiste Jonglez (improvements, conversion to Zola, hosting on Inria Gitlab)

For contact please send an email to Baptiste Jonglez (firstname.lastname at inria dot fr)

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License (CC-BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/).
