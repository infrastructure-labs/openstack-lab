#!/usr/bin/env python3

# $ pipenv run setup  [--test/--no-test]

import logging
import socket
import itertools
import time

import click

import enoslib
from enoslib.api import (play_on, ensure_python3)
from enoslib.infra.enos_g5k.provider import G5k
from enoslib.infra.enos_g5k.configuration import (Configuration)


enoslib.init_logging(level=logging.INFO)

LAB = "lab-2024-imta-fise-login-nuage"
TEAMS = [
	"bjonglez",
	"dbalouek",
	"aparion",
	"ajebari",
	"cpinton",
	"ccaugant",
	"elassara",
	"fyahyaoui",
	"jdai",
	"lreynaud",
	"mbaraton",
	"magarnier",
	"mdallel",
	"mkerara",
	"ncanavy",
	"rmoyeuvre",
	"rmaillar",
	"sfrossar",
	"szakaria",
	"truan",
	"tdreves",
	"tsautere",
	"wjemli",
	"zuzhang",
]


def get_ip_addr(h: enoslib.Host) -> str:
    'Returns the IP address of a Host `h`'
    try:
        return f'{socket.gethostbyname(h.address)}, {h.address}'
    except socket.gaierror:
        return h.address


def make_conf(testing=True) -> Configuration:
    conf = {
        "reservation": "2024-03-13 08:40:00",
        "walltime": "08:10:00",
        "job_name": LAB + "_microstack",
        "project": LAB,
        "env_name": "ubuntu2204-min",
        "resources": {
            "networks": [
                {
                    "id": "net",
                    "type": "prod",
                    "roles": ["network_interface"],
                    "site": "nancy",
                },
                # Keep this for future work, for a deployment
                # based OpenStack.
                # {
                #     # Note: *NEVER* assigns this to a machine!
                #     "id": "net_ext",
                #     "type": "slash_22",
                #     "roles": ["neutron_external_interface"],
                #     "site": "rennes",
                # },
            ],
            "machines": [
                {
                    "roles": ["OpenStack"],
                    "cluster": "gros",
                    "nodes": len(TEAMS),
                    "primary_network": "net",
                    "secondary_networks": [ ],
                }
            ],
        }
    }

    if testing:
        del(conf["reservation"])
        conf["walltime"] = "01:30:00"
        conf["resources"]["machines"][0]["nodes"] = 1

    return Configuration.from_dictionnary(conf)


def provision(rs: enoslib.Roles):
    with play_on(roles=rs, pattern_hosts="OpenStack") as p:
        # Install the bare necessities
        p.raw('apt update')
        p.apt(pkg=['curl', 'htop', 'tcpdump', 'lynx', 'vim', 'kmod'],
              update_cache=True)

        # IP Forwarding
        p.raw('sysctl -w net.ipv4.ip_forward=1')

        # Setup ssh for root w/ password
        p.raw('echo "root:lab-os" | chpasswd')
        p.blockinfile(path='/etc/ssh/sshd_config',
                      block="PasswordAuthentication yes\nPermitRootLogin yes\n")
        p.systemd(name='ssh', state='restarted')

        # Enhance default bash
        for l in ('. /etc/bash_completion',         # Offer bash completion
                  'export PATH=/snap/bin:${PATH}',  # Put /snap/bin in PATH
                  ) :
            p.lineinfile(path='/root/.bashrc', line=l)



# Main
@click.command()
@click.option('--test/--no-test', default=False)
def main(test):
    # Config
    enoslib.set_config(g5k_auto_jump=False)
    # Claim the resources
    infra = G5k(make_conf(testing=test))
    roles, networks = infra.init(force_deploy=False)

    # Provision machines
    provision(roles)

    # Display subnet
    subnet = networks["neutron_external_interface"]
    print(subnet)

    # Assign machines
    print("Lab machine assignations:")
    addrs = map(get_ip_addr, roles["OpenStack"])
    for (usr, addr) in itertools.zip_longest(TEAMS, addrs):
        print(f"- {addr} ({usr}): ")


if __name__ == "__main__":
    main()
