+++
title = "Setup access"
weight = 2
+++

In this last part, the teacher has setup an OpenStack in a datacenter
(here, on top of Grid'5000) and created regular member account and project
for each of you.  You are no longer admin on OpenStack!

As a preamble, you should connect to the Grid'5000 VPN.

- [Install the OpenStack CLI](https://github.com/openstack/python-openstackclient/tree/stable/wallaby#getting-started) and [Heat CLI](https://github.com/openstack/python-heatclient/tree/stable/wallaby).
  You will need a shell and a working Python environment on your machine.
  Here is an example for Linux with a virtualenv:

```bash
$ python3 -m venv venv-openstack-lab
$ source venv-openstack-lab/bin/activate
$ pip install python-openstackclient python-heatclient
```

- Download and extract the [source files](../../lab-os.tar.gz) for the lab on your computer.
- Go on the Horizon dashboard of your teacher's OpenStack (the IP address
  should be on the shared pad) and download the "OpenStack RC File" (see
  [previous part](@/openstack-admin/cli-create-vm.md)) on your own
  machine.
  + user name: your Grid'5000 login
  + password: `lab-os`
- Source this "OpenStack RC File".
- Create your admin SSH key (see [previous part about SSH
  keys](@/openstack-admin/ssh-keys.md)).

Resource names change a bit from the previous part of the lab.
Do not hesitate to run some commands such as the following to know about the new names.

```bash
openstack network list
openstack image list
openstack flavor list
```

You are now ready to create resources through Heat!
