+++
title = "Automated orchestration with Heat"
weight = 3
sort_by = "weight"
insert_anchor_links = "right"
+++

[Heat](https://docs.openstack.org/heat/ussuri/) is the OpenStack
orchestrator: it eats templates (called HOT for Heat Orchestration
Template - which are files written in YAML) describing the OpenStack
infrastructure you want to deploy (e.g. VMs, networks, storages) as well
as software configurations.  Then the Heat engine is in charge of sending
the appropriate requests to OpenStack to deploy the system described in
your template (deployments are called `stacks` in Heat).  This section
manipulates Heat to understand how to deploy applications on OpenStack.
Templates in the following are available under the `openstack-user-heat/`
directory of the [lab-os.tar.gz](../../lab-os.tar.gz) tarball.  You may
also find interesting examples in the [Heat
documentation](https://docs.openstack.org/heat/ussuri/template_guide/basic_resources.html),
or on the [heat-templates
repository](https://github.com/openstack/heat-templates).
