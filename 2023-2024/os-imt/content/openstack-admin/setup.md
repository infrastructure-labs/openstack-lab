+++
title = "Initial setup"
weight = 1
+++

## Environment

OpenStack needs at least 6 GB of RAM to run. And plenty more to start VMs
on it. Therefore, this lab relies on
[Grid'5000](https://www.grid5000.fr/), a testbed for experimental
research, to acquire a *Lab machine* larger than your personal one. The
Lab machine is a Ubuntu 22.04 with 96Go of RAM and 36 CPU cores. This
should be enough resources for this lab!

The lab makes use of [Snap
microstack](https://github.com/CanonicalLtd/microstack): OpenStack in a
Snap that you can run locally on a single machine. Snap is the Canonical's
App delivery mechanism. It enables developers to bundle all dependencies
into a single app package. And so does Snap microstack for an all-in-one
OpenStack on your machine.

An all-in-one OpenStack means that your machine will contain both services
to *operate* and *host* virtualized resources. For instance, the
`nova-conductor` to operate the boot of a VM, and `nova-compute` to host
the VM. This is a good setup for a lab, but not for production. There are
several other options such as
[DevStack](https://docs.openstack.org/devstack/latest/index.html),
[Puppet-OpenStack](https://docs.openstack.org/puppet-openstack-guide/latest/)
or [Kolla-ansible](https://docs.openstack.org/developer/kolla-ansible/)
and all matters. But, Snap microstack takes only 2 minutes to deploy
OpenStack (instead of 30 minutes for other options).

{% note() %}
- Devstack is good for OpenStack developers.
- Puppet-OpenStack or Kolla-ansible are good for production
  deployments.
{% end %}

## Access the Lab machine

Find the assignation list of Lab machine per student on the collaborative pad.

First thing first, you have to connect to the Lab machine.  Unfortunately,
the Lab machine isn't available publicly, but hides behind the Grid'5000
private network. One solution, as explained in the [official
tutorial](https://www.grid5000.fr/mediawiki/index.php/Getting_Started#Connecting_for_the_first_time),
consists in opening an SSH connection to the publicly available
`access.grid5000.fr` machine, and from there, doing a second SSH
connection to the Lab machine. But this solution is fairly limited since
it doesn't give an access to services from your own
machine.

To ease the interaction between your own machine and the Lab one, you
should setup the Grid'5000
[VPN](https://en.wikipedia.org/wiki/Virtual_private_network).  The
Grid'5000 VPN gives you access to the Grid'5000 private network and thus,
Lab machines wherever your are on the globe.

Next gives you the procedure on Ubuntu, but should be similar on other
UNIX systems. You can also do it on Windows, however expect to be on
your own in case of troubles.

1. Install the [OpenVPN](https://openvpn.net/) client:
```bash
sudo apt update -y
sudo apt install -y openvpn
```
3. Go on [UMS](https://api.grid5000.fr/stable/users/) > "My Account" tab >
   "VPN certificates" item.
4. "Create new certificate" > "Create with passphrase", and fill the
   form with a new password (remember it!).
5. Click on "Zip file" Action > store it somewhere on your personal
   machine > unzip it. If the `unzip` program is unavailable, install
   it with `sudo apt install unzip`.  Uncompress with:
```bash
unzip <g5k_login>_vpnclient.zip -d g5k_vpnclient
```
6. Run OpenVPN client with sudo:
```bash
cd g5k_vpnclient
sudo openvpn Grid5000_VPN.ovpn
```
7. Fill in the password of Step 3 to the question `Enter Private Key
   Password:`.

You have correctly setup the VPN and can access Grid'5000 private network
if your **shell hangs** and you see the following route in your routing
table.
```bash
$ ip route
# ...
10.0.0.0/8 via 172.20.255.254 dev tun0
172.16.0.0/16 via 172.20.255.254 dev tun0
172.20.0.0/16 via 172.20.255.254 dev tun0
172.20.192.0/18 dev tun0 proto kernel scope link src 172.20.192.5
# ...
```

You can finally connect to your Lab machine in another shell with the
following SSH command. Use `lab-os` as password.
```bash
ssh -l root <ip-of-your-lab-machine>
```

The rest of this lab **proceeds on the Lab machine**.

Don't forget to use tmux! (see the pad)

## Resources of the lab {#lab-resources}

Get the resources of the lab at <https://infrastructure-labs.gitlabpages.inria.fr/openstack-lab/2023-2024/os-imt/lab-os.tar.gz>

```bash
wget https://infrastructure-labs.gitlabpages.inria.fr/openstack-lab/2023-2024/os-imt/lab-os.tar.gz
tar xzf lab-os.tar.gz
cd openstack-admin
```

The archive contains:
- **setup.sh**: script that sets up the lab.
- **teardown.sh**: script that uninstalls the lab.
- **other scripts**: other bash scripts useful for the lab.

## Setup OpenStack
On Ubuntu 22.04, the `snap` package manager is already installed.

Install the latest version of OpenStack from the snap store, as root:
```bash
snap install microstack --beta --devmode
```

Execute the `setup.sh` file as root to initialize OpenStack (setup
networks, flavors, images, ...).
```bash
./setup.sh
```

{% do() %}
Then, ensure OpenStack services are running on your machine. Find the
snap command that lists microstack OpenStack services and the
status? What is the purpose of each service?
{% end %}

{% solution() %}

```bash
snap services microstack | sort
```

- **`glance-*`**: Glance to manage VM images: `openstack image --help`.
- **`horizon-*`**: OpenStack Web dashboard: `https://<ip-of-your-lab-machine>`.
- **`keystone-*`**: Keystone to manage authentication and authorization on
  OpenStack.
- **`neutron-*`**: Neutron to manage networks: `openstack network --help`.
- **`nova-*`**: Nova to manage VM: `openstack server --help`.
- **`memcached`**: Cache used by all OpenStack services
- **`mysqld`**: Database used by all OpenStack services
- **`rabbitmq-server`**: Communication bus used by all OpenStack services
{% end %}
